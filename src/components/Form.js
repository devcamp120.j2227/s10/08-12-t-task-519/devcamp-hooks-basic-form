import { useState } from "react";

const Form = () => {
    const [firstname, setFirstname] = useState(localStorage.getItem("firstname") || "");
    const [lastname, setLastname] = useState(localStorage.getItem("lastname") || "");

    const inputFirstnameChangeHandler = (event) => {
        localStorage.setItem("firstname", event.target.value);
        
        setFirstname(event.target.value);
    }

    const inputLastnameChangeHandler = (event) => {
        localStorage.setItem("lastname", event.target.value);

        setLastname(event.target.value);
    }

    return (
        <div>
            <input placeholder="Input Firstname" value={firstname} onChange={inputFirstnameChangeHandler}></input>
            <br></br>
            <input placeholder="Input Lastname" value={lastname} onChange={inputLastnameChangeHandler}></input>
            <br></br>
            <p>{lastname} {firstname}</p>
        </div>
    )
}

export default Form;