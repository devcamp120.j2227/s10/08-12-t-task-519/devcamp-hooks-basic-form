import Form from "./components/Form";

function App() {
  return (
    <div style={{margin: "5px"}}>
      <Form />
    </div>
  );
}

export default App;
